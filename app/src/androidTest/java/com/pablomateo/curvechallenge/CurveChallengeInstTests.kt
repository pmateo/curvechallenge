package com.pablomateo.curvechallenge

import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by pablomateofdez on 14/05/2018.
 */
@RunWith(AndroidJUnit4::class)
class CurveChallengeInstTests {

    /**
     * Instrumental Testing
     *  TESTS:
     *      - checkSumInt: To avoid accidental type changes of the variable
     *      - resultTextChanges: Checks that the Results TextView gets updated if any of the 6 fields is changed
     *      - ifEmptyTextValueZero: Makes sure value is zero if no numbers are added to the fields
     *      - stringsNotAllowed: Checks that the fields don´t accept strings as input
     *      - resultTextSumsUp: Checks that the total sum is the sum of the values from the 6 fields.
     *
     * PARAMS:
     *      @param numberFields Array with the number fields IDs
     */

    @Rule
    @JvmField
    val mainActivityRule = ActivityTestRule(MainActivity::class.java)

    val numberFields = arrayOf( R.id.number_field1,
            R.id.number_field2,
            R.id.number_field3,
            R.id.number_field4,
            R.id.number_field5,
            R.id.number_field6)

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        Assert.assertEquals("com.pablomateo.curvedevchallenge", appContext.packageName)
    }

    @Test
    fun checkSumInt() {
        //Test to avoid accidental type changes
        val totalSumValue = SumModel()
        val sumIsInt = if (totalSumValue.valueToSum is Int) {
            true
        } else { false }
        Assert.assertEquals(true, sumIsInt)
    }

    @Test
    fun resultTextChanges() {
        val textToType = "1"
        for (numFieldId in numberFields) {
            Espresso.onView(ViewMatchers.withId(numFieldId))
                    .perform(ViewActions.typeText(textToType), ViewActions.closeSoftKeyboard())
            Espresso.onView(ViewMatchers.withId(R.id.result_text))
                    .check(ViewAssertions.matches(ViewMatchers.withText(textToType)))
            Espresso.onView(ViewMatchers.withId(numFieldId))
                    .perform(ViewActions.clearText(), ViewActions.closeSoftKeyboard())
        }
    }

    @Test
    fun ifEmptyTextValueZero() {
        val textToType = ""
        val mainViewModel = MainViewModel()
        Espresso.onView(ViewMatchers.withId(R.id.number_field1))
                .perform(ViewActions.typeText(textToType), ViewActions.closeSoftKeyboard())
        Assert.assertEquals(0, mainViewModel.numFieldET1.valueToSum)
    }

    @Test
    fun stringsNotAllowed() {
        val textToType = "a"
        val mainViewModel = MainViewModel()
        Espresso.onView(ViewMatchers.withId(R.id.number_field1))
                .perform(ViewActions.typeText(textToType), ViewActions.closeSoftKeyboard())
        Assert.assertEquals(0, mainViewModel.numFieldET1.valueToSum)
    }

    @Test
    fun resultTextSumsUp() {
        //Test with different options
        val sumNumbers: Int = 2
        val textToType = sumNumbers.toString()
        val expedtedResult = (sumNumbers * numberFields.count()).toString()

        for (numFieldId in numberFields) {
            Espresso.onView(ViewMatchers.withId(numFieldId))
                    .perform(ViewActions.typeText(textToType), ViewActions.closeSoftKeyboard())
        }
        Espresso.onView(ViewMatchers.withId(R.id.result_text)).check(ViewAssertions.matches(ViewMatchers.withText(expedtedResult)))
    }

    //>adb shell dumpsys gfxinfo <PACKAGE_NAME> framestats
    //Use this to check frames statistics for Results Text Blinking Animation (if needed)

}