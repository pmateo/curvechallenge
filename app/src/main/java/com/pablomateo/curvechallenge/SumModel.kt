package com.pablomateo.curvechallenge

import android.databinding.ObservableField
import android.text.Editable
import android.text.TextWatcher
import java.util.*

/**
 * Created by pablomateofdez on 14/05/2018.
 */

class SumModel {

    var valueToSum: Int = 0
    var text = ObservableField<String>()

    var watcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (!Objects.equals(text.get(), s.toString())) {
                text.set(s.toString())

                //Lifted if statement (Kotlin prefers it rather than -> if(condition){} else{})
                valueToSum = if (text.get().toString() != "") {
                    text.get().toInt()
                } else { 0 }
            }
        }

        override fun afterTextChanged(s: Editable) { }
    }
}