package com.pablomateo.curvechallenge

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.pablomateo.curvechallenge.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /**
         * Data-Binding
         */
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        binding.viewModel = MainViewModel()


        /**
         * EXTRAS
         * Different implementation for Flashing the Total
         *      To use, comment the onClick property of the result_text TextView in activity_main.xml
         *      Uncomment the following block comment
         */

        /*
        var resultTextAnimating = false
        result_text.setOnClickListener {
            if (!resultTextAnimating) {
                val anim = AlphaAnimation(0.0f, 1.0f)
                anim.duration = 500
                anim.repeatMode = Animation.REVERSE
                anim.repeatCount = Animation.INFINITE
                result_text.startAnimation(anim)
            } else {
                result_text.clearAnimation()
            }
            resultTextAnimating = !resultTextAnimating
        }
        */
    }
}
