package com.pablomateo.curvechallenge

import android.databinding.ObservableField
import android.databinding.BaseObservable
import android.view.View
import android.view.animation.Animation
import android.view.animation.AlphaAnimation

/**
 * Created by pablomateofdez on 14/05/2018.
 */
class MainViewModel: BaseObservable() {

    /**
     * VIEWMODEL
     *
     * We could choose between different approaches.
     * I thought if it was better 1 watcher for multiple EditTexts or the opposite
     * Finally, I decided it was cleaner and more efficient to have multiple instances of a single watcher for each ET.
     * The watcher is included in the model for better consistency
     * This way, the logic is global, but you can change any ET individually in the future changing to another class instance.
     */


    var numFieldET1 = SumModel()
    var numFieldET2 = SumModel()
    var numFieldET3 = SumModel()
    var numFieldET4 = SumModel()
    var numFieldET5 = SumModel()
    var numFieldET6 = SumModel()

    var resultString = ObservableField("Result")

    fun updateSum() {
        val totalSum =  numFieldET1.valueToSum +
                numFieldET2.valueToSum +
                numFieldET3.valueToSum +
                numFieldET4.valueToSum +
                numFieldET5.valueToSum +
                numFieldET6.valueToSum

        resultString.set("$totalSum")
    }


    //Blink Animation for Total Sum TextField
    fun onSumTotalClick(view: View) {
        val animation = view.animation
        if (animation == null) {
            view.startAnimation(getAnimation())
        } else {
            animation.cancel()
            view.animation = null
        }
    }

    fun getAnimation(): AlphaAnimation {
        val anim = AlphaAnimation(0.0f, 1.0f)
        anim.duration = 500
        anim.repeatMode = Animation.REVERSE
        anim.repeatCount = Animation.INFINITE
        return anim
    }

}