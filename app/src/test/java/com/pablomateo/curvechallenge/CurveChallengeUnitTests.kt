package com.pablomateo.curvechallenge

import org.junit.Assert
import org.junit.Test

/**
 * Created by pablomateofdez on 14/05/2018.
 */
class CurveChallengeUnitTests {

    /**
     * Basic Unit Testing
     * Check CurveChallengeInstTests.kt for in depth testing
     */

    @Test
    fun checkSumInt() {
        //Test to avoid accidental type changes
        val totalSumValue = SumModel()
        val sumIsInt = if (totalSumValue.valueToSum is Int) {
            true
        } else { false }
        Assert.assertEquals(true, sumIsInt)
    }
}